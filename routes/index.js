const auth = require('./auth');
const lk = require('./lk');
const admin = require('./admin');

module.exports = {
  auth,
  lk,
  admin
};