const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt-nodejs');

const models = require('../models');

const BlindSignature = require('../blind-signatures');

const cryptico = require('cryptico');

// index lk
router.get('/', (req, res) => {
  models.User.find({}, function (err, users) {
    var exportData = {};
    exportData.users = users;
    models.Vote.find({}, function (err, votes) {
      exportData.votes = votes;
      res.render('lk', {
        exportData: exportData,
        user: req.session
      });
    })
  })
});

// vote on questions
router.get('/vote', (req, res) => {
  models.User.find({}, function (err, users) {
    var exportData = {};
    exportData.users = users;
    models.Vote.findOne({_id: req.query.voteid})
      .then(vote => {
        exportData.vote = vote;
        res.render('vote', {
          exportData: exportData,
          user: req.session,
          voteid: req.query.voteid
        })
      })
      .catch(err => {
        res.send('Ошибка, голосование не найдено');
      });
  })
});

// view result of vote
router.get('/result-vote', (req, res) => {
  models.Bulletin.find({voteId: req.query.voteid})
    .then(bulletins => {
      var voteResult = [];
      if (bulletins.length === 0) {
        res.send('Голосование не найдено');
      } else {
        bulletins.forEach(function (bulletin, key) {
          bulletin.response.forEach(function (question, index) {
            if (key === 0) {
              voteResult[index] = {
                title: '',
                options: []
              }
            }
            question.inputValue.forEach(function (input) {
              if (voteResult[index].title === '') {
                voteResult[index].title = question.title;
                voteResult[index].options.push(input);
              } else {
                voteResult[index].options.push(input);
              }
            })
          })
        })
      }
      voteResult.forEach(function (question) {
        question.total = question.options.reduce(function(acc, el) {
          acc[el] = (acc[el] || 0) + 1;
          return acc;
        }, {});
      });

      res.render('total', {
        title: 'Результаты голосования',
        totalVote: voteResult,
      })
    })
    .catch(err => {
      console.log(err);
    })
});

// check bulletin
router.get('/check', (req, res) => {

  models.Bulletin.findOne({'identHash': req.query.uniqueid})
    .then(bulletin => {
      res.render('check', {
        title: 'Бюллетень',
        bulletenResponse: bulletin.response
      })
    });


});


// ajax for register on vote
router.post('/register-participant', (req, res) => {
  if (req.body.status === 'start') {
    var voteid = req.body.voteid;
    models.Vote.findOne({_id: voteid}, function (err, vote) {
      var encryptedId = [];
      vote.uniqueId.forEach(function (item) {
        var cipher = BlindSignature.sign(item, vote.key.N, vote.key.E);
        encryptedId.push(cipher.toString());
      });
      res.json({
        signed: true,
        ok: true,
        encryptedId: encryptedId,
        N: vote.key.N,
        E: vote.key.E
      });
    });
  } else if (req.body.status === 'blinded') {
    models.Vote.findOne({_id: req.body.voteid}, function (err, vote) {
      vote.activeParticipants.push(req.session.userId);
      vote.save(function (err, array) {
        console.log('Участник зарегистрировался на голосование.');
      });
      var decrypted = BlindSignature.sign(req.body.blinded, vote.key.N, vote.key.D);
      res.json({
        ok: true,
        decrypted: decrypted.toString(),
        voteId: vote.id
      });
    });
  }
});

// ajax for save bulletin
router.post('/send-vote', (req, res) => {
  var serverRSAkey = cryptico.generateRSAKey(req.body.passPhrase, req.body.bits);
  var DecryptionResult = cryptico.decrypt(req.body.cipher, serverRSAkey);
  var fixedResult = decodeURIComponent(escape(DecryptionResult.plaintext));
  var acceptedBulletin = JSON.parse(fixedResult);
  models.Vote.findOne({_id: acceptedBulletin.voteid})
    .then(vote => {
      vote.votedParticipants.push(req.session.userId);
      vote.save();
    })
    .catch(() => {
      console.log('Ошибка добавления проголосовавшего');
    });
  models.Bulletin.create({
    identHash: acceptedBulletin.uniqueId,
    response: acceptedBulletin.answers,
    voteId: acceptedBulletin.voteid
  })
    .then(() => {
      console.log('Бюллетень успешно получен и сохранен');
      res.json({
        ok: true
      })
    })
    .catch(() => {
      res.json({
        ok: false
      })
    })
});


module.exports = router;