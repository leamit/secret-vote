const express = require("express");
const router = express.Router();
// const bcrypt = require('bcrypt-nodejs');
const BlindSignature = require("../blind-signatures");

const models = require("../models");

// GET for logout
router.get("/", (req, res) => {
  models.User.find({}, function(err, users) {
    var exportData = {};
    exportData.users = users;
    models.Vote.find({}, function(err, votes) {
      exportData.votes = votes;
      res.render("admin", {
        exportData: exportData
      });
    });
  });
});

router.get("/delete-vote", (req, res) => {
  models.Vote.findOne({ _id: req.query.voteId }, function(err, vote) {
    vote.remove();
    res.redirect("/admin");
  });
});

router.post("/add-vote", (req, res) => {
  console.log(req.body);

  const requestVote = req.body;

  var bigInt = require("big-integer");

  var keyObject = BlindSignature.keyGeneration({ b: 128 });
  var keyProperties = BlindSignature.keyProperties(keyObject);

  console.log("Создано новое голосование!");

  const uniqueId = [];
  for (var i = 0; i < requestVote.participants.length * 10; i++) {
    uniqueId.push(bigInt.randBetween("1e10", "1e20").toString(10));
  }

  models.Vote.create({
    name: requestVote.voteName,
    registerTime: requestVote.registerTime,
    date: requestVote.date,
    duration: requestVote.duration,
    participants: requestVote.participants,
    questions: requestVote.questions,
    activeParticipants: [],
    votedParticipants: [],
    uniqueId,
    key: keyProperties
  })
    .then((newVote) => {
      console.log(`Добавлено новое голосование – ${newVote.name}`);
      res.json({
        ok: true
      });
    })
    .catch(err => {
      console.log(err);
      res.json({
        ok: false,
        error: 'Ошибка, попробуйте позже!'
      });
    });
});

router.post("/register", (req, res) => {
  const login = req.body.login;
  const name = req.body.name;
  const role = "user";
  const password = req.body.password;
  const repeatPassword = req.body.repeatPassword;

  if (!login || !password || !repeatPassword) {
    res.json({
      ok: false,
      error: "Все поля должны быть заполнены!",
      fields: ["login", "password", "repeatPassword"]
    });
  } else if (login.length < 3 || login.length > 16) {
    res.json({
      ok: false,
      error: "Длина логина от 3 до 16 символов!",
      fields: ["login"]
    });
  } else if (password !== repeatPassword) {
    res.json({
      ok: false,
      error: "Пароли не совпадают!",
      fields: ["password", "repeatPassword"]
    });
  } else {
    models.User.findOne({
      login
    }).then(user => {
      if (!user) {
        bcrypt.hash(password, null, null, (err, hash) => {
          models.User.create({
            login,
            name,
            role,
            password: hash
          })
            .then(user => {
              console.log("Зарестрирован новый пользователь" + user.login);
              res.json({
                ok: true
              });
            })
            .catch(err => {
              console.log(err);
              res.json({
                ok: false,
                error: "Ошибка, попробуйте позже!"
              });
            });
        });
      } else {
        res.json({
          ok: false,
          error: "Имя занято!",
          fields: ["login"]
        });
      }
    });
  }
});

module.exports = router;
