$(function () {
  // print uniqueID
  function getVoteIdFromUrl() {
    var url_string = window.location.href;
    var url = new URL(url_string);
    return url.searchParams.get("voteid");
  }

  var voteIdFromGet;
  $('.js-uniqueid-message span').text(window.localStorage.getItem(getVoteIdFromUrl()));

  //register
  $('.register-user').on('click', function (e) {
    e.preventDefault();
    var $form = $(this).parents('form');
    var data = {
      login: $form.find('input[name="login"]').val(),
      name: $form.find('input[name="name"]').val(),
      password: $form.find('input[name="password"]').val(),
      repeatPassword: $form.find('input[name="repeat-password"]').val()
    }

    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/api/auth/register'
    })
      .done(function (data) {
        location.reload();
      });

    return false;
  });

  // login
  $('.login-button').on('click', function (e) {
    e.preventDefault();
    var $form = $(this).parents('form');

    var data = {
      login: $form.find('input[name="login"]').val(),
      password: $form.find('input[name="password"]').val(),
    };

    console.log(data);

    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/api/auth/login'
    }).done(function (data) {
      if (!data.ok) {
        $('.login h2').after('<p class="error">' + data.error + '</p>');
        if (data.fields) {
          data.fields.forEach(function (item) {
            $('input[name=' + item + ']').addClass('error');
          });
        }
      } else {
        $(location).attr('href', '/');
      }
    });
  });

  //register on vote
  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  $('.vote__item-register').one('click', function () {
    console.log('Начало регистрации');
    var voteid = $(this).data('voteid');
    var message = {
      status: 'start',
      voteid: voteid
    };

    $.ajax({
      type: 'POST',
      data: JSON.stringify(message),
      contentType: 'application/json',
      url: '/lk/register-participant',
      success: function (data) {
        if (data.ok) {
          console.log('Сервер ответил');
          var BigInteger = jsbn.BigInteger;
          var randomId = getRandomInt(0, data.encryptedId.length);
          var message = new BigInteger(data.encryptedId[randomId]);
          var N = new BigInteger(data.N);
          var E = new BigInteger(data.E);

          var bigOne = new BigInteger('1');
          var gcd;
          var r;

          do {
            r = new BigInteger(secureRandom(64)).mod(N);
            gcd = r.gcd(N);
          } while (
            !gcd.equals(bigOne) ||
            r.compareTo(N) >= 0 ||
            r.compareTo(bigOne) <= 0
            );
          var blinded = message.multiply(r.modPow(E, N)).mod(N);

          var clientData = {
            status: 'blinded',
            blinded: blinded.toString(),
            voteid: voteid
          };

          $.ajax({
            type: 'POST',
            data: JSON.stringify(clientData),
            contentType: 'application/json',
            url: '/lk/register-participant',
            success: function (data) {
              // var buttonParent = $(this).parent();
              // $(this).remove();
              // $('<p>Успешно зарегистрированы. Ожидайте начала голосования.</p>').appendTo(buttonParent);
              // console.log(data);
              var BigInteger = jsbn.BigInteger;
              data.decrypted = new BigInteger(data.decrypted);
              var unblinded = data.decrypted.multiply(r.modInverse(N)).mod(N);
              // console.log(unblinded.toString());
              console.log('Регистрация успешна. Уникальный идентификатор получен.');
              window.localStorage.setItem(data.voteId, unblinded);
              location.reload();
            }
          });
        }
      }
    });

    return false;
  });

  // Send vote
  $('.js-send-vote').on('click', function () {
    var answers = [];
    $(this).parent().find('.voting__item').each(function (index, item) {
      var inputValue = [];
      if ($(item).find('input[type="checkbox"]').length !== 0) {
        $(item).find('input[type="checkbox"]:checked').each(function (key, value) {
          inputValue.push(value.value);
        })
      } else if ($(item).find('textarea').length !== 0) {
        inputValue.push($(item).find('textarea').val());
      } else {
        inputValue.push($(item).find('input').val());
      }
      answers[index] = {
        title: $(item).find('.voting__item-title').text(),
        inputValue: inputValue
      }
    });
    var bulletin = {
      voteid: getVoteIdFromUrl(),
      uniqueId: window.localStorage.getItem(getVoteIdFromUrl()),
      answers: answers
    };

    // console.log(JSON.stringify(bulletin));

    // encrypt bulletin
    var PassPhrase = Math.random().toString(36).substr(2, 9);
    var Bits = 512;
    var clientRSAkey = cryptico.generateRSAKey(PassPhrase, Bits);
    var clientPublicKeyString = cryptico.publicKeyString(clientRSAkey);
    var EncryptionResult = cryptico.encrypt(unescape(encodeURIComponent(JSON.stringify(bulletin))), clientPublicKeyString);

    var voteResult = {
      identHash: window.localStorage.getItem(getVoteIdFromUrl()),
      cipher: EncryptionResult.cipher,
      voteid: getVoteIdFromUrl(),
      passPhrase: PassPhrase,
      bits: Bits
    };

    console.log(PassPhrase);

    $.ajax({
      type: 'POST',
      data: JSON.stringify(voteResult),
      contentType: 'application/json',
      url: '/lk/send-vote',
      success: function (data) {
        if (data) {
          window.location.href='/lk';
        }
      }
    });

    return false;
  });


//  accordion
  $('.vote__accordion-title').on('click', function () {
    var $this = $(this);
    $this.toggleClass('open');
    $this.siblings('ul').slideToggle();
    return false;
  })

});


/* eslint-enable no-undef */
