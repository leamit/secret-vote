const express = require('express');

const secureRandom = require('secure-random');
const BigInteger = require('jsbn').BigInteger;
const sha256 = require('js-sha256');
const NodeRSA = require('node-rsa');

function keyGeneration(params) {
  const key = new NodeRSA(params || { b: 2048 });
  return key;
}

function keyProperties(key) {
  return {
    E: new BigInteger(key.keyPair.e.toString()).toString(),
    N: new BigInteger(key.keyPair.n.toString()).toString(),
    D: new BigInteger(key.keyPair.d.toString()).toString()
  };
}

function messageToHash(message) {
  const messageHash = sha256(message);
  return messageHash;
}

function messageToHashInt(message) {
  const messageHash = messageToHash(message);
  const messageBig = new BigInteger(messageHash, 16);
  return messageBig;
}

function blind({ message, key, N, E }) {
  const messageHash = messageToHashInt(message);
  N = key ? key.keyPair.n : new BigInteger(N.toString());
  E = key
    ? new BigInteger(key.keyPair.e.toString())
    : new BigInteger(E.toString());

  const bigOne = new BigInteger('1');
  let gcd;
  let r;
  do {
    r = new BigInteger(secureRandom(64)).mod(N);
    gcd = r.gcd(N);
  } while (
    !gcd.equals(bigOne) ||
    r.compareTo(N) >= 0 ||
    r.compareTo(bigOne) <= 0
    );
  const blinded = messageHash.multiply(r.modPow(E, N)).mod(N);
  return {
    blinded,
    r,
  };
}

function sign(blinded, N, D) {
  // const { N, D } = keyProperties(key);
  // const { N, D } = key;
  // blinded = new BigInteger(blinded.toString());
  blinded = new BigInteger(blinded);
  var NBI = new BigInteger(N);
  var DBI = new BigInteger(D);
  const signed = blinded.modPow(DBI, NBI);
  return signed;
}

function unblind({ signed, key, r, N }) {
  r = new BigInteger(r.toString());
  N = key ? key.keyPair.n : new BigInteger(N.toString());
  signed = new BigInteger(signed.toString());
  const unblinded = signed.multiply(r.modInverse(N)).mod(N);
  return unblinded;
}

function verify({ unblinded, key, message, E, N }) {
  unblinded = new BigInteger(unblinded.toString());
  const messageHash = messageToHashInt(message);
  N = key ? key.keyPair.n : new BigInteger(N.toString());
  E = key
    ? new BigInteger(key.keyPair.e.toString())
    : new BigInteger(E.toString());

  const originalMsg = unblinded.modPow(E, N);
  const result = messageHash.equals(originalMsg);
  return result;
}

function verify2({ unblinded, key, message }) {
  unblinded = new BigInteger(unblinded.toString());
  const messageHash = messageToHashInt(message);
  const { D, N } = keyProperties(key);
  const msgSig = messageHash.modPow(D, N);
  const result = unblinded.equals(msgSig);
  return result;
}

module.exports = {
  keyGeneration,
  keyProperties,
  messageToHash,
  blind,
  sign,
  unblind,
  verify,
  verify2
};