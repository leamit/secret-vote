$(() => {
  $('#addNewVote').on('click', () => {
    $('#new-vote').slideToggle();
  });

  $('.new-vote__add').on('click', () => {
    // var newQuestion = $('.new-vote__questions-item:first').clone().appendTo('.new-vote__questions-wrap');
    const questionHtml = '<div class="new-vote__questions-item">\n'
      + '                            <div class="new-vote__questions-title">Вопрос #<span>1</span></div>\n'
      + '                            <input type="text" placeholder="Заголовок вопроса" name="questionTitle">\n'
      + '                            <textarea name="questionDesc" placeholder="Описание вопроса" rows="4"></textarea>\n'
      + '                            <select name="questionType" class="new-vote__questions-type">\n'
      + '                                <option value="checkbox">Чекбокс</option>\n'
      + '                                <option value="radio">Радио</option>\n'
      + '                                <option value="input">Строка</option>\n'
      + '                                <option value="textarea">Текстовое поле</option>\n'
      + '                            </select>\n'
      + '                            <div class="new-vote__options">\n'
      + '                                <div class="new-vote__questions-subtitle">Варианты</div>\n'
      + '                                <div class="new-vote__options-container">\n'
      + '                                    <input type="text">\n'
      + '                                    <input type="text">\n'
      + '                                </div>\n'
      + '                                <button class="js-add-option">Добавить вариант</button>\n'
      + '                                <button class="js-remove-option" style="display: none">Удалить вариант</button>\n'
      + '                            </div>\n'
      + '                            <button class="new-vote__del">Удалить вопрос</button>\n'
      + '                        </div>';
    const newQuestion = $(questionHtml).appendTo('.new-vote__questions-wrap');
    $(newQuestion)
      .find('.new-vote__questions-title span')
      .text($('.new-vote__questions-wrap').children().length);
    return false;
  });

  $(document).on('change', '.new-vote__questions-type', function () {
    const $this = $(this);
    if ($this.val() === 'checkbox' || $this.val() === 'radio') {
      $this.siblings('.new-vote__options').slideDown();
    } else {
      $this.siblings('.new-vote__options').slideUp();
    }
  });

  $(document).on('click', '.js-add-option', function () {
    $('<input type="text">').appendTo(
      $(this).siblings('.new-vote__options-container'),
    );
    $(this)
      .siblings('.js-remove-option')
      .show();
    return false;
  });
  $(document).on('click', '.js-remove-option', function () {
    $(this)
      .siblings('.new-vote__options-container')
      .find('input:last')
      .remove();
    if (
      $(this)
        .siblings('.new-vote__options-container')
        .find('input').length < 3
    ) {
      $(this).hide();
    }
    return false;
  });

  $(document).on('click', '.new-vote__del', function () {
    $(this)
      .parents('.new-vote__questions-item')
      .remove();
    return false;
  });

  $('input[name="date"]').datepicker({
    minDate: new Date(),
    timepicker: true,
    startDate: new Date(),
    todayButton: new Date(),
    position: 'top left',
    // dateFormat: 'yyyy-mm-ddThh:mm:ss.sssZ',
    onSelect(formattedDate, date, inst) {
      $('#new-vote input[name="dateUnix"]').val(date.getTime());
    },
  });

  $('#send-vote').on('click', () => {
    const $form = $('#new-vote').find('form');
    const participants = [];
    const questions = [];

    $form.find('.new-vote__participants input').each((index, item) => {
      if ($(item).is(':checked')) {
        participants.push($(item).attr('name'));
      }
    });

    $form.find('.new-vote__questions-item').each((index, item) => {
      questions.push({});
      questions[index].title = $(item)
        .find('input[name="questionTitle"]')
        .val();
      questions[index].description = $(item)
        .find('textarea[name="questionDesc"]')
        .val();
      questions[index].type = $(item)
        .find('select[name="questionType"]')
        .val();
      questions[index].options = [];
      if (
        questions[index].type === 'checkbox'
        || questions[index].type === 'radio'
      ) {
        $(item)
          .find('.new-vote__options input')
          .each((i, item) => {
            questions[index].options.push(item.value);
            // console.log(questions[index].options);
          });
      }
    });

    const voteData = {
      voteName: $form.find('input[name="voteName"]').val(),
      registerTime: $form.find('input[name="registerTime"]').val(),
      date: $form.find('input[name="dateUnix"]').val(),
      duration: $form.find('input[name="duration"]').val(),
      participants,
      questions,
    };
    console.log(voteData);

    $.ajax({
      type: 'POST',
      data: JSON.stringify(voteData),
      contentType: 'application/json',
      url: '/admin/add-vote',
    }).done((data) => {
      if (data.ok) {
        location.reload();
      }
    });

    return false;
  });
});
