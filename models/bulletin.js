const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema ({
  identHash: {
    type: String,
    required: true,
    unique: true
  },
  response: {
    type: Array,
    required: true
  },
  voteId: {
    type: String,
    required: true
  }
}, {
  timeStamp: true
});

schema.set('toJSON', {
  virtuals: true
});

module.exports = mongoose.model('Bulletin', schema);