const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema ({
  name: {
    type: String,
    required: true,
    unique: true
  },
  registerTime: {
    type: Number,
    required: true
  },
  date: {
    type: Object,
    required: true
  },
  duration: {
    type: Number,
    required: true
  },
  participants: {
    type: Array,
    required: true
  },
  activeParticipants: {
    type: Array,
    required: true
  },
  votedParticipants: {
    type: Array,
    required: true
  },
  questions: {
    type: Array,
    required: true
  },
  uniqueId: {
    type: Array,
    required: true
  },
  key: {
    type: Object,
    required: true
  }
}, {
  timeStamp: true
});

schema.set('toJSON', {
  virtuals: true
});

module.exports = mongoose.model('Vote', schema);