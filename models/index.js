// const Post = require('./question');
const User = require('./user');
const Bulletin = require('./bulletin');
const Vote = require('./vote');

module.exports = {
  Vote,
  User,
  Bulletin
};