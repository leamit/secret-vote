// const express = require('express');
// const bodyParser = require('body-parser');
// const path = require('path');
//
// const app = express();
//
// const Question = require('./models/question');
//
// app.set('view engine', 'ejs');
// app.use(bodyParser.urlencoded({extended: true}));
// app.use(express.static(path.join(__dirname, 'dist')));
// app.use(
//   '/javascripts',
//   express.static(path.join(__dirname, 'node_modules', 'jquery', 'dist'))
// );
//
// app.get('/', function (req, res) {
//   Question.find({})
//     .then(posts => {
//       res.render('index', {posts: posts});
//     });
// });
//
// app.get('/create', function (req, res) {
//   res.render('create');
// });
// app.post('/create', function (req, res) {
//   const {title, body} = req.body;
//
//   Question.create({
//     title: title,
//     body: body
//   }).then(question => console.log(question.id));
//   res.redirect('/');
// });
//
// module.exports = app;

const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const staticAsset = require("static-asset");
const mongoose = require("mongoose");
const session = require("express-session");
const MongoStore = require("connect-mongo")(session);
const config = require("./config");
const routes = require("./routes");

// database
mongoose.Promise = global.Promise;
mongoose.set("debug", config.IS_PRODUCTION);
mongoose.connection
  .on("error", error => console.log(error))
  .on("close", () => console.log("Database connection closed."))
  .once("open", () => {
    const info = mongoose.connections[0];
    console.log(`Connected to ${info.host}:${info.port}/${info.name}`);
  });
mongoose.connect(config.MONGO_URL, {
  useNewUrlParser: true,
  useCreateIndex: true
});

// express
const app = express();

// sessions
app.use(
  session({
    secret: config.SESSION_SECRET,
    resave: true,
    saveUninitialized: false,
    store: new MongoStore({
      mongooseConnection: mongoose.connection
    })
  })
);

// sets and uses
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(staticAsset(path.join(__dirname, "dist")));
app.use(staticAsset(path.join(__dirname, "favicon")));
app.use(express.static(path.join(__dirname, "dist")));
app.use(express.static(path.join(__dirname, "favicon")));
app.use(
  "/javascripts",
  express.static(path.join(__dirname, "node_modules", "jquery", "dist"))
);
app.use(
  "/javascripts",
  express.static(path.join(__dirname, "node_modules", "js-sha256", "src"))
);
app.use(
  "/javascripts",
  express.static(path.join(__dirname, "node_modules", "secure-random", "lib"))
);
app.use(
  "/javascripts",
  express.static(path.join(__dirname, "node_modules", "jsbn"))
);
app.use("/cryptico-js", express.static("cryptico"));

// routers
app.get("/", (req, res) => {
  const userInfo = {
    id: req.session.userId,
    name: req.session.userName,
    role: req.session.userRole
  };

  res.render("index", { userInfo });
});

app.use("/api/auth", routes.auth);

app.use("/lk", routes.lk);

app.use("/admin", routes.admin);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
// eslint-disable-next-line no-unused-vars
app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.render("error", {
    message: error.message,
    error: !config.IS_PRODUCTION ? error : {}
  });
});

app.listen(config.PORT, () => {
  console.log(`Secret vote working on localhost:${config.PORT}!`);
});
